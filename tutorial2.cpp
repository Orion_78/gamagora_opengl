#define SAMEBUFFER
#define NUMBERTOSPAWN 10

#include "App.h"
#include "Widgets/nvSDLContext.h"

#include "Mesh.h"
#include "MeshIO.h"

#include "GL/GLQuery.h"
#include "GL/GLTexture.h"
#include "GL/GLBuffer.h"
#include "GL/GLVertexArray.h"
#include "ProgramManager.h"

//! classe utilitaire : permet de construire une chaine de caracteres formatee. cf sprintf.
struct Format
{
    char text[1024];
	
    
    Format( const char *_format, ... )
    {
        text[0]= 0;     // chaine vide
        
        // recupere la liste d'arguments supplementaires
        va_list args;
        va_start(args, _format);
        vsnprintf(text, sizeof(text), _format, args);
        va_end(args);
    }
    
    ~Format( ) {}
    
    // conversion implicite de l'objet en chaine de caracteres stantard
    operator const char *( )
    {
        return text;
    }
};


//! squelette d'application gKit.
class TP : public gk::App
{
	float rotate ;
	float distance;

    nv::SdlContext m_widgets;
    
    gk::GLProgram *m_program;
    gk::GLVertexArray *m_vao;
    
    gk::GLBuffer *m_vertex_buffer;
    gk::GLBuffer *m_index_buffer;
    int m_indices_size;
    
    gk::GLCounter *m_time;
    
public:
    // creation du contexte openGL et d'une fenetre
    TP( )
        :
        gk::App()
    {
        // specifie le type de contexte openGL a creer :
        gk::AppSettings settings;
        settings.setGLVersion(3,3);     // version 3.3
        settings.setGLCoreProfile();      // core profile
        settings.setGLDebugContext();     // version debug pour obtenir les messages d'erreur en cas de probleme
        
        // cree le contexte et une fenetre
        if(createWindow(512, 512, settings) < 0)
            closeWindow();
        
        m_widgets.init();
        m_widgets.reshape(windowWidth(), windowHeight());
    }
    
    ~TP( ) {}
    
    int init( )
    {
		rotate = 0;
		distance = 50;


		/*
		* TEXTURES
		*/



        // compilation simplifiee d'un shader program
        gk::programPath("shaders");
        m_program = gk::createProgram("myDiffuseShader.glsl");
        if(m_program == gk::GLProgram::null())
            return -1;
        
        // charge un mesh
        //gk::Mesh *mesh= gk::MeshIO::readOBJ("bbox.obj");
		gk::Mesh *mesh= gk::MeshIO::readOBJ("bigguy.obj");
        if(mesh == NULL)
            return -1;
        
        // cree le vertex array objet, description des attributs / associations aux variables du shader
        m_vao= gk::createVertexArray();

		std::vector<gk::Vec3> m2;// = mesh->positions;
		int s =  mesh->positions.size();
		
		for(int i =0; i < s; i++)
		{
			mesh->positions.push_back( mesh->positions[i] );
			mesh->positions[i].x += 0;
		}

		
        // cree le buffer de position
        m_vertex_buffer= gk::createBuffer(GL_ARRAY_BUFFER,  mesh->positions );
        // associe le contenu du buffer a la variable 'position' du shader
        glVertexAttribPointer(m_program->attribute("vPosition"), 3, GL_FLOAT, GL_FALSE, 0, 0);
        // active l'utilisation du buffer 
        glEnableVertexAttribArray(m_program->attribute("vPosition"));

		// cree le buffer des normals
		gk::createBuffer(GL_ARRAY_BUFFER,  mesh->normals );
        // associe le contenu du buffer a la variable 'position' du shader
        glVertexAttribPointer(m_program->attribute("vNormal"), 3, GL_FLOAT, GL_FALSE, 0, 0);
        // active l'utilisation du buffer 
        glEnableVertexAttribArray(m_program->attribute("vNormal"));

		


        
        // cree le buffer d'indices et l'associe au vertex array
        m_index_buffer= gk::createBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indices);
        // conserve le nombre d'indices (necessaire pour utiliser glDrawElements)
        m_indices_size= mesh->indices.size();
        
        // mesh n'est plus necessaire, les donnees sont transferees dans les buffers sur la carte graphique
        delete mesh;
        
        // nettoyage de l'etat opengl
        glBindVertexArray(0);   // desactive le vertex array
        glBindBuffer(GL_ARRAY_BUFFER, 0);       // desactive le buffer de positions
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);       // desactive le buffer d'indices
        
        // mesure du temps de dessin
        m_time= gk::createTimer();


		
        
        // ok, tout c'est bien passe
        return 0;
    }
    
    int quit( )
    {
        return 0;
    }

    // a redefinir pour utiliser les widgets.
    void processWindowResize( SDL_WindowEvent& event )
    {
        m_widgets.reshape(event.data1, event.data2);
    }
    
    // a redefinir pour utiliser les widgets.
    void processMouseButtonEvent( SDL_MouseButtonEvent& event )
    {
        m_widgets.processMouseButtonEvent(event);
    }
    
    // a redefinir pour utiliser les widgets.
    void processMouseMotionEvent( SDL_MouseMotionEvent& event )
    {
        m_widgets.processMouseMotionEvent(event);
    }
    
    // a redefinir pour utiliser les widgets.
    void processKeyboardEvent( SDL_KeyboardEvent& event )
    {
        m_widgets.processKeyboardEvent(event);
    }

	

	void drawOneObject(gk::Vector modelTranslate, float modelRotate)
	{
		 // transformations
		gk::Transform model= gk::RotateY(modelRotate) * gk::Translate(modelTranslate); 

        // composition des transformations
        gk::Transform mv= view * model;
        gk::Transform mvp= projection * mv;
        gk::Transform mvpv= viewport * mvp;

		// Passage au shader
		m_program->uniform("posCamera") = view.inverse( gk::Point(0,0,0) );
		m_program->uniform("posLight") = posLight;
		m_program->uniform("modelInverse") = model.inverseMatrix();
        m_program->uniform("lightColor")= gk::VecColor(1, 1, 1); 
		m_program->uniform("mvpMatrix")= mvp.matrix();
		m_program->uniform("shininess")= 20.0f;


        // dessiner un maillage indexe
        glDrawElements(GL_TRIANGLES, m_indices_size, GL_UNSIGNED_INT, 0);
	}

	gk::Vector camPosition;
	gk::Transform view;
	gk::Vec3 posLight;
    gk::Transform projection;
    gk::Transform viewport;
    
    int draw( )
    {
        input();
        
        //
        glViewport(0, 0, windowWidth(), windowHeight());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // mesurer le temps d'execution
        m_time->start();
        
        /* configuration minimale du pipeline :
            glBindVertexArray() // association du contenu des buffers aux attributs declares dans le shader
            glUseProgram()  // indique quelle paire de shaders utiliser
            { ... } // parametrer les uniforms des shaders

            glDraw()    // execution du pipeline
         */

        // selectionner un ensemble de buffers et d'attributs de sommets
        glBindVertexArray(m_vao->name);

        // dessiner quelquechose
        glUseProgram(m_program->name);


		//light= light * gk::RotateY(100);
		camPosition = gk::Vector(0,0,-100);
		// Create light
		posLight= gk::Vec3( rotate,0,distance );
		view = gk::Translate( camPosition );
        projection= gk::Perspective(50.f, 1.f, 1.f, 1000.f);
        viewport= gk::Viewport(windowWidth(), windowHeight());

#ifdef SAMEBUFFER
		for (int i=0; i<NUMBERTOSPAWN; i++)
		{
			//drawOneObject(gk::Vector(0,0,0),0);
			drawOneObject(gk::Vector(i*10,0,0),0);

		}
#endif

		
        
        // nettoyage
        glUseProgram(0);
        glBindVertexArray(0);
        
        // mesurer le temps d'execution
        m_time->stop();
        
        // afficher le temps d'execution
        {
            m_widgets.begin();
            m_widgets.beginGroup(nv::GroupFlags_GrowDownFromLeft);
            
            m_widgets.doLabel(nv::Rect(), m_time->summary("draw").c_str());
            
            m_widgets.endGroup();
            m_widgets.end();
        }
        
        // afficher le dessin
        present();
        // continuer
        return 1;
    }


	void input()
	{
		if(key(SDLK_ESCAPE))
            // fermer l'application si l'utilisateur appuie sur ESCAPE
            closeWindow();
        
        if(key('r'))
        {
            key('r')= 0;
            // recharge et recompile les shaders
            gk::reloadPrograms();
        }
        
        if(key('c'))
        {
            key('c')= 0;
            // enregistre l'image opengl
            gk::writeFramebuffer("screenshot.png");
        }

		if(key(SDLK_LEFT))
            rotate-= 1.f;
        if(key(SDLK_RIGHT))
            rotate+= 1.f;
        if(key(SDLK_UP))
            distance+= 1.f;
        if(key(SDLK_DOWN))
            distance-= 1.f;
	}
};


int main( int argc, char **argv )
{
    TP app;
    app.run();
    
    return 0;
}

