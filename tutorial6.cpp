
// affiche un cube texture en 2 passes : la premiere passe remplit la texture, et la 2ieme passe dessine le cube en utilisant la texture de la premiere etape. utilise un framebuffer object.


#include "App.h"
#include "Widgets/nvSDLContext.h"

#include "Mesh.h"
#include "MeshIO.h"

#include "Image.h"
#include "ImageIO.h"

#include "GL/GLQuery.h"
#include "GL/GLTexture.h"
#include "GL/GLSampler.h"
#include "GL/GLFramebuffer.h"
#include "GL/GLBasicMesh.h"
#include "ProgramManager.h"

#include "ProgramTweaks.h"

//! classe utilitaire : permet de construire une chaine de caracteres formatee. cf sprintf.
struct Format
{
    char text[1024];
    
    Format( const char *_format, ... )
    {
        text[0]= 0;     // chaine vide
        
        // recupere la liste d'arguments supplementaires
        va_list args;
        va_start(args, _format);
        vsnprintf(text, sizeof(text), _format, args);
        va_end(args);
    }
    
    ~Format( ) {}
    
    // conversion implicite de l'objet en chaine de caracteres stantard
    operator const char *( )
    {
        return text;
    }
};


//! squelette d'application gKit.
class TP : public gk::App
{
    nv::SdlContext m_widgets;
    
    gk::GLProgram *m_program_contruct_texture;
	gk::GLProgram *m_program_ShadowMap;
	gk::GLProgram *m_program_ShadowTexture;
    gk::GLProgram *m_program_texture;
    gk::GLBasicMesh *m_mesh;
    gk::GLBasicMesh *m_quad;
    gk::GLFramebuffer *m_framebuffer;
    gk::GLTexture *m_texture;
	gk::GLTexture *m_textureCreated;
    gk::GLSampler *m_sampler;
    
    gk::GLCounter *m_time;

	gk::Transform view;
	gk::Transform light;
	gk::Transform model;

	gk::Transform mvpLight;
    
public:
    // creation du contexte openGL et d'une fenetre
    TP( )
        :
        gk::App()
    {
        // specifie le type de contexte openGL a creer :
        gk::AppSettings settings;
        settings.setGLVersion(3,3);     // version 3.3
        settings.setGLCoreProfile();      // core profile
        settings.setGLDebugContext();     // version debug pour obtenir les messages d'erreur en cas de probleme
        
        // cree le contexte et une fenetre
        if(createWindow(512, 512, settings) < 0)
            closeWindow();
        
        m_widgets.init();
        m_widgets.reshape(windowWidth(), windowHeight());
    }
    
    ~TP( ) {}
    
    int init( )
    {
		


		 // charge un mesh
		gk::Mesh *mesh= gk::MeshIO::readOBJ("bigguy.obj");
        if(mesh == NULL)
            return -1;
		       
        // charge un mesh
  //      gk::Mesh *mesh= gk::MeshIO::readOBJ("cube.obj");
   //     if(mesh == NULL)
   //         return -1;
		
		
	

		

        // compilation simplifiee d'un shader program
        gk::programPath("shaders");
        // affichage du cube texture
        m_program_texture= gk::createProgram("texture.glsl");
        if(m_program_texture == gk::GLProgram::null())
            return -1;

		 
       


		/* construction de la texture du cube (un damier noir et blanc)
        m_program_contruct_texture= gk::createProgram("damier.glsl");
        if(m_program_texture == gk::GLProgram::null())
            return -1;*/
        

		// construction de la texture du cube (un damier noir et blanc)
        m_program_ShadowMap= gk::createProgram("myShadowMap.glsl"); 
        if(m_program_ShadowMap == gk::GLProgram::null())
            return -1;
		
		// construction de la texture du cube (un damier noir et blanc)
        m_program_ShadowTexture= gk::createProgram("myShadowTexture.glsl");
        if(m_program_ShadowTexture == gk::GLProgram::null())
            return -1;



			
        

		 // cree le vertex array objet, description des attributs / associations aux variables du shader
        // utilise un gk::GLBasicMesh qui permet de creer et de configurer les buffers directement
        m_mesh= new gk::GLBasicMesh(GL_TRIANGLES, mesh->indices.size());
        m_mesh->createBuffer(m_program_texture->attribute("position").location, mesh->positions);
        m_mesh->createBuffer(m_program_texture->attribute("texcoord").location, mesh->texcoords);
        m_mesh->createBuffer(m_program_texture->attribute("normal").location, mesh->normals);
        m_mesh->createIndexBuffer(mesh->indices);

		// mesh n'est plus necessaire, les donnees sont transferees dans les buffers sur la carte graphique
        delete mesh;







        


		
        
       
        
        // nettoyage de l'etat opengl
        glBindVertexArray(0);                       // desactive le vertex array, cree par gk::GLBasicMesh
        glBindBuffer(GL_ARRAY_BUFFER, 0);           // desactive le buffer de positions
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   // desactive le buffer d'indices
        
		
		
        // construit et configure un framebuffer object : la sortie 0 est associee a une texture couleur 128x128 (la texture est aussi cree).
        m_framebuffer= gk::createFramebuffer(GL_FRAMEBUFFER, 2048, 2048, gk::GLFramebuffer::COLOR0_BIT | gk::GLFramebuffer::DEPTH_BIT);
		// recupere la texture creee par le framebuffer
        //m_textureCreated= m_framebuffer->texture(gk::GLFramebuffer::COLOR0);
		m_textureCreated= m_framebuffer->texture(gk::GLFramebuffer::DEPTH);


		 


        // charge une image et cree une texture
        gk::Image *image= gk::ImageIO::readImage("bigguy_ambient.png", 4);    // construit une image avec 4 canaux rgba
        m_texture= gk::createTexture2D(0, image);       // texture 0












        
        
    #if 0       // version openGL directe
        // 1. cree la texture
        // genere un identifiant
        GLuint texture_name;
        glGenTextures(1, &texture_name);        
        
        // selection l'entree 0
        glActiveTexture(GL_TEXTURE0);
        
        // construit une texture 2d non initalisee
        glBindTexture(GL_TEXTURE_2D, texture_name);
        glTexImage2D(GL_TEXTURE_2D, 0, 
            GL_RGBA, 512, 512, 0,
            GL_RGBA, GL_UNSIGNED_BYTE,  NULL); 
        glGenerateMipmap(GL_TEXTURE_2D);
        
        // 2. cree le framebuffer et associe la texture a la sortie 0
        // genere un identifiant
        GLuint framebuffer_name;
        glGenFramebuffers(1, framebuffer_name);
        
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_name);
        // associe la texture a la sortie 0
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture_name, 0);
    #endif
    
        // nettoyage
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        
        // construit un  pseudo maillage pour remplir la texture
        m_quad= new gk::GLBasicMesh(GL_TRIANGLE_STRIP, 4);
        
        // parametre un objet sampler pour acceder a la texture
        m_sampler= gk::createDepthSampler();
        
        // mesure du temps de dessin
        m_time= gk::createTimer();

		// Init Input
		distance= 50;
		rotation= 45;

        
        // ok, tout c'est bien passe
        return 0;
    }
    
    int quit( )
    {
        delete m_mesh;
        return 0;
    }

    // a redefinir pour utiliser les widgets.
    void processWindowResize( SDL_WindowEvent& event )
    {
        m_widgets.reshape(event.data1, event.data2);
    }
    
    // a redefinir pour utiliser les widgets.
    void processMouseButtonEvent( SDL_MouseButtonEvent& event )
    {
        m_widgets.processMouseButtonEvent(event);
    }
    
    // a redefinir pour utiliser les widgets.
    void processMouseMotionEvent( SDL_MouseMotionEvent& event )
    {
        m_widgets.processMouseMotionEvent(event);
    }
    
    // a redefinir pour utiliser les widgets.
    void processKeyboardEvent( SDL_KeyboardEvent& event )
    {
        m_widgets.processKeyboardEvent(event);
    }

	 // a redefinir pour utiliser les widgets.
    void processTextEvent( const char *string )
    {
        m_widgets.processTextEvent(string);
    }
    
	
	// deplace et re-oriente le cube
    float distance;
    float rotation;

	void getInput()
	{
		if(key(SDLK_ESCAPE))
            // fermer l'application si l'utilisateur appuie sur ESCAPE
            closeWindow();
        
        if(key('r'))
        {
            key('r')= 0;
            // recharge et recompile les shaders
            gk::reloadPrograms();
        }
        
        if(key('c'))
        {
            key('c')= 0;
            // enregistre l'image opengl
            gk::writeFramebuffer("screenshot.png");
        }
        
       
        
        if(key(SDLK_LEFT))
            rotation++;
        if(key(SDLK_RIGHT))
            rotation--;
        
        if(key(SDLK_UP))
            distance= distance + 1;
        if(key(SDLK_DOWN))
            distance= distance - 1;



		view =   gk::Translate( gk::Vector( 0, 0, -50 ) ) ;
		light = gk::RotateX(90) * gk::Translate( gk::Vector( 0, -50, 0 ) )  ;
		
		model = gk::RotateY( rotation ) * gk::Translate( gk::Vector( 0, 0, -distance ) );

	}


	/*
	void draw_CubeTexture()
	{
		 // change la taille du damier a chaque frame
        static int frame= 0;
        frame= (frame + 1) % (m_texture->width / 4);
        
		// etape 1 : remplir la texture / le damier
        // activer le framebuffer configure pour dessiner dans la texture
        glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer->name);
        // selectionne les sorties 
        GLenum buffers[]= { GL_COLOR_ATTACHMENT0 };
        glDrawBuffers(1, buffers);
        
        // configure la taille de l'image a dessiner
        glViewport(0, 0, m_texture->width, m_texture->height);
        // efface l'image avant de dessiner
        glClear(GL_COLOR_BUFFER_BIT);
        // pas de zbuffer associe au framebuffer, on peut desactiver le test de profondeur
        glDisable(GL_DEPTH_TEST);
        
        // selectionne le shader program pour construire / dessiner le maillage
        glUseProgram(m_program_texture->name);
        
        m_program_texture->uniform("taille")= frame;
        m_quad->draw();

		// nettoyage : reconfigure le pipeline pour dessiner dans la fenetre
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glEnable(GL_DEPTH_TEST);
	}
	*/

	
	

	void draw_MeshWithTexture()
	{        
		// efface l'image
        glViewport(0, 0, windowWidth(), windowHeight());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // dessiner quelquechose
		glUseProgram(m_program_texture->name);
        
        // transformations
        // Model position on input
		// View on input
        gk::Transform projection= gk::Perspective( 50, 1, 1, 100 );
        gk::Transform mv= view * model;
        gk::Transform mvp= projection * mv;
        
        // parametrer le shader
        m_program_texture->uniform("mvpMatrix")= mvp.matrix();      // transformation model view projection
        m_program_texture->uniform("normalMatrix")= mv.normalMatrix();      // transformation des normales
        m_program_texture->uniform("diffuse_color")= gk::VecColor(1, 1, 1);
		m_program_texture->uniform("mvpMatrixLight")= mvpLight.matrix();
        
        // associer l'entree 0 et le sampler du shader

//		glSamplerParameterf(0, "diffuse_texture", GL_TEXTURE_BORDER_COLOR);

        m_program_texture->sampler("diffuse_texture")= 0;
        
        // activer l'objet texture sur l'entree 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_texture->name);
        glGenerateMipmap(GL_TEXTURE_2D);
        // activer l'objet sampler sur l'entree 0
        glBindSampler(0, m_sampler->name);

        // utilise l'utilitaire draw de gk::GLBasicMesh
        m_mesh->draw();
	}

	void draw_Shadow()
	{

		// etape 1 : remplir la texture / le damier
        // activer le framebuffer configure pour dessiner dans la texture
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer->name);

        // selectionne les sorties 
        GLenum buffers[]= { GL_COLOR_ATTACHMENT0 };
        glDrawBuffers(1, buffers);
        
        // configure la taille de l'image a dessiner
        glViewport(0, 0, m_textureCreated->width, m_textureCreated->height);

	
        // efface l'image avant de dessiner
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        
        // selectionne le shader program pour construire / dessiner le maillage
        glUseProgram(m_program_ShadowMap->name);
		gk::Transform projection= gk::Perspective( 50, 1, 1, 100 );
        gk::Transform mv= light * model;
        mvpLight= projection * mv;
        
        // parametrer le shader
        m_program_ShadowMap->uniform("mvpMatrix")= mvpLight.matrix();      // transformation model view projection
		m_program_ShadowMap->uniform("diffuse_color")= gk::Vec4(1, 1, 1, 1);

		
		// view on input
//        gk::Transform projection= gk::Perspective( 50, 1, 1, 100 );
//        gk::Transform viewport= gk::Viewport(windowWidth(), windowHeight());

       // m_program_texture->uniform("taille")= frame;
		m_mesh->draw();

		// nettoyage : reconfigure le pipeline pour dessiner dans la fenetre
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}

	void draw_meshWithShadow()
	{
		// efface l'image
        glViewport(0, 0, windowWidth(), windowHeight());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // dessiner quelquechose
		glUseProgram(m_program_ShadowTexture->name);
        
        // transformations
        // Model position on input
		// View on input
        gk::Transform projection= gk::Perspective( 50, 1, 1, 100 );
        gk::Transform mv= view * model;
        gk::Transform mvp= projection * mv;
        
        // parametrer le shader
		m_program_ShadowTexture->uniform("mvpMatrix")= mvp.matrix();      // transformation model view projection
      //  m_program_ShadowTexture->uniform("normalMatrix")= mv.normalMatrix();      // transformation des normales
        m_program_ShadowTexture->uniform("diffuse_color")= gk::VecColor(1, 1, 1);
		m_program_ShadowTexture->uniform("mvpMatrixLight")= mvpLight.matrix();
        
        // associer l'entree 0 et le sampler du shader
        m_program_ShadowTexture->sampler("shadow_texture")= 0;
        
        // activer l'objet texture sur l'entree 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_textureCreated->name);
        glGenerateMipmap(GL_TEXTURE_2D);

        // activer l'objet sampler sur l'entree 0
        glBindSampler(0, m_sampler->name);

		gk::tweakProgram(m_program_ShadowTexture);

        // utilise l'utilitaire draw de gk::GLBasicMesh
        m_mesh->draw();
	}
	
    int draw( )
    {
		getInput();

        // mesurer le temps d'execution
        m_time->start();
        
		draw_Shadow();
	//	draw_MeshWithTexture();
		draw_meshWithShadow();

		glBindFramebuffer(GL_READ_FRAMEBUFFER, m_framebuffer->name);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glBlitFramebuffer( 0,0, m_textureCreated->width, m_textureCreated->height, 0,0, 200, 200, GL_COLOR_BUFFER_BIT, GL_LINEAR);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

        // nettoyage
        glUseProgram(0);
        glBindVertexArray(0);
        glBindSampler(0, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
        
        // mesurer le temps d'execution
        m_time->stop();
        
        // afficher le temps d'execution
        {
            m_widgets.begin();
            m_widgets.beginGroup(nv::GroupFlags_GrowDownFromLeft);
            
            m_widgets.doLabel(nv::Rect(), m_time->summary("draw").c_str());

			gk::doTweaks(&m_widgets, m_program_ShadowTexture);
            
            m_widgets.endGroup();

			// affiche l'interface de modifications d'uniforms
            

            m_widgets.end();
        }
        
        // afficher le dessin
        present();
        // continuer
        return 1;
    }
};


int main( int argc, char **argv )
{
    TP app;
    app.run();
    
    return 0;
}