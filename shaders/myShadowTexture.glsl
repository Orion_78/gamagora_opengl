#version 330

#ifdef VERTEX_SHADER
    uniform mat4 mvpMatrix;
  //  uniform mat4 normalMatrix;



    
    layout(location= 0) in vec3 position;           //!< attribut
   // layout(location= 1) in vec3 texcoord;           //!< attribut
    layout(location= 2) in vec3 normal;             //!< attribut
    
    out vec3 vertex_normal;
   // out vec2 vertex_texcoord;
    out vec3 fragment_pos;
 //   out vec4 fragment_pos_ForShadow;

    
    void main( )
    {
        gl_Position= mvpMatrix * vec4(position, 1.0);
        fragment_pos = position;

       /* mat4 biasMatrix(
        0.5, 0.0, 0.0, 0.0,
        0.0, 0.5, 0.0, 0.0,
        0.0, 0.0, 0.5, 0.0,
        0.5, 0.5, 0.5, 1.0
        );*/

        //fragment_pos_ForShadow = mvpMatrixLight * vec4(position, 1.0);
        
       // vertex_texcoord= texcoord.st;


        vertex_normal=  normal; // vertex_normal= mat3(normalMatrix) * normal;
        
    }
#endif

#ifdef FRAGMENT_SHADER
    uniform vec4 diffuse_color;
    uniform sampler2D shadow_texture;
    uniform mat4 mvpMatrixLight;

   

  //  uniform mat4 mvpMatrix;



//matextureDistanceLight
//mat4 view * projection de la source de lumiere

    in vec3 fragment_pos;
    in vec3 vertex_normal;
  //  in vec2 vertex_texcoord;
    
 //    vec4 fragment_pos_ForShadow; //in


    out vec4 fragment_color;

   

    void main( )
    {
       fragment_color = vec4(0,0,1,0);

      
      vec3 n = normalize(vertex_normal);

      vec4 moved_p = mvpMatrixLight * vec4(fragment_pos + n*0.2,1);
       
      vec4 texcoord = (moved_p / moved_p.w)* 0.5 + 0.5;
  

     vec4 t = texture(shadow_texture, texcoord.xy);

        if( 
          texcoord.z > t.x )
        {
           //fragment_color.rgb = vec3(,0,0);
          fragment_color.rgb= diffuse_color.rgb * abs(normalize(vertex_normal).z) ; 
          fragment_color.rgb *= 0.2;
        }
        else
        {
             fragment_color.rgb= diffuse_color.rgb * abs(normalize(vertex_normal).z) ; 
        }

//      fragment_color.rg= texcoord.xy;
   //   fragment_color.rgb= texture(shadow_texture, texcoord.xy).xxx;
      /*
      OLD TRY
        fragment_pos_ForShadow = mvpMatrixLight * fragment_pos;
        vec2 test = (fragment_pos_ForShadow.xy / fragment_pos_ForShadow.w) * 0.5 + 0.5;

        

        if (  (fragment_pos_ForShadow.z / fragment_pos_ForShadow.w) * 0.5 + 0.5  < texture(shadow_texture, vertex_texcoord).x )
        {
            fragment_color= fragment_color * 0.2;
    //      fragment_color.rgb= fragment_color.rgb * 0.2;
     //   }diffuse_color.rgb * abs(normalize(vertex_normal).z) ;
        }
        else
        {
          //fragment_color.rgb= vec3(0,1,0);
        }
     //   else
     //   {
     //       fragment_color.rgb = vec3(1,0,0);
     //   }
*/



      //  vec4 diffuse= texture(shadow_texture, vertex_texcoord);
	
    /*   if (  texture( shadow_texture, fragment_pos_ForShadow.xy ).z  <  fragment_pos_ForShadow.z){
            fragment_color.rgb= diffuse_color.rgb * abs(normalize(vertex_normal).z) ; 
        }
        else

        {

         fragment_color.rgb = vec3(1,0,0);
        }
*/

/*






        vec4 p = fragment_pos_ForShadow;

        
   /*     if ( (   p.w < p.x || p.w > p.x ||
             p.w < p.y || p.w > p.y ||
             p.w < p.z || p.w > p.z ) ) 
        {
            // out of the projected light
            fragment_color.rgb = vec3(1,1,1);
        }
        else
        {*/
            /*
            // Move to projected light
            p.x = p.x/p.z;
            p.y = p.y/p.z;
            p.z = p.z/p.z;

            // change from -1 - 1 to 0 - 1
            p.x = p.x * 0.5 + 0.5;
            p.y = p.y * 0.5 + 0.5;
            p.z = p.z * 0.5 + 0.5;
            

            vec4 zbuffer = texture( shadow_texture, p.xy );

            if (fragment_pos.z < zbuffer.z)
            {
                fragment_color.rgb= diffuse_color.rgb * abs(normalize(vertex_normal).z) ;    
            }
            else
            {
                fragment_color.rgb = vec3(1,0,0);
            }
*/
            
  //      }

        // point visible par la lumière ?
        // correspondance du point dans l'espace lumière
        // 

        
        //q.z <>zbuffer
        //0 < q.x < q.w -1
        //0 < q.y < q.w
    ////    if (q.r < zbuffer.z)
   ///     {

        
   ////     }
   ///     else

   ///     {

   ///         fragment_color.rgb= diffuse_color.rgb * abs(normalize(vertex_normal).z) * 0;

   ///     }
        //q = pixel (q.x / q.w, q.y / q.w)



        
    }
#endif

