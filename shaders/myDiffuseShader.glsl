#version 430

#ifdef VERTEX_SHADER
    layout (location= 0) in vec3 vPosition;
    layout (location= 1) in vec3 vNormal;

    uniform mat4 mvpMatrix;
    uniform mat4 modelInverse;

    uniform vec3 posCamera;
    uniform vec3 posLight;

    out vec3 position;
    out vec3 normal;

    out vec3 posLocalCamera;
    out vec3 posLocalLight;

    void main()
    {
        posLocalCamera = (modelInverse * vec4(posCamera,1)).xyz;
        posLocalLight = (modelInverse * vec4(posLight,1)).xyz;

        normal = vNormal;
        position = vPosition;

        gl_Position = mvpMatrix * vec4(vPosition, 1.0);
    }
#endif

#ifdef FRAGMENT_SHADER
    out vec3 outputColor;

    uniform vec4 lightColor;
    uniform float shininess;

    in vec3 position;
    in vec3 normal;

    in vec3 posLocalCamera;
    in vec3 posLocalLight;

    void main()
    {
        vec3 lightDir =  normalize( posLocalLight - position);
        vec3 camDir =  normalize( posLocalCamera - position);
        vec3 h = normalize( lightDir + camDir );
		vec3 n= normalize(normal);
        float theta = max(0,dot(n,lightDir));
        float thatah = max(0,dot(n,h));
        float fr = (shininess+1)/(2*3.14) * pow(thatah,shininess);
        outputColor = 0.5*lightColor.rgb * theta * fr + 0.5*lightColor.rgb * theta;
    }
#endif