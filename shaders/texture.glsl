#version 330

#ifdef VERTEX_SHADER
    uniform mat4 mvpMatrix;
    uniform mat4 normalMatrix;


    
    layout(location= 0) in vec3 position;           //!< attribut
    layout(location= 1) in vec3 texcoord;           //!< attribut
    layout(location= 2) in vec3 normal;             //!< attribut
    
    out vec3 vertex_normal;
    out vec2 vertex_texcoord;
    
    void main( )
    {
        gl_Position= mvpMatrix * vec4(position, 1.0);
        vertex_normal= mat3(normalMatrix) * normal;
        vertex_texcoord= texcoord.st;
    }
#endif

#ifdef FRAGMENT_SHADER
    uniform vec4 diffuse_color;
    uniform sampler2D diffuse_texture;
    
//matextureDistanceLight
//mat4 view * projection de la source de lumiere


    in vec3 vertex_normal;
    in vec2 vertex_texcoord;
    
    out vec4 fragment_color;

    void main( )
    {
        vec4 diffuse= texture(diffuse_texture, vertex_texcoord);
	
        ///q = mvpMatrix * p

//zbuffer =  texture(shadow, q.z)
//q.z <>zbuffer
//0 < q.x < q.w -1
//0 < q.y < q.w

//q = pixel (q.x / q.w, q.y / q.w)


        fragment_color.rgb= diffuse_color.rgb * abs(normalize(vertex_normal).z) * diffuse.rgb;
    }
#endif

