2014 - Gamagora, Lyon


Draw models, textures and cast shadows using Opengl with gKit made by [jean-claude Iehl](http://liris.cnrs.fr/membres/?id=1200) and Shaders.

Works done by myself are in the tutorial 2 and 6.

Tutorial 2 show in a convenient way multiple object using a self made diffuse Shader :

![light shader.png](https://bitbucket.org/repo/qXRgpd/images/339912414-light%20shader.png)

Tutorial 6 show two things.

A model drawed with a texture :

![texture.png](https://bitbucket.org/repo/qXRgpd/images/644190616-texture.png)

And a Shadow map (light view in the bottom left corner) :

![shadow.png](https://bitbucket.org/repo/qXRgpd/images/1868482585-shadow.png)

======= Programmation Lancer de rayon sur Shader =======

Les Compute Shader

Pas dans le pipline graphique standard (aucune automatisation), donc utile pour tout ce qui n'est pas affichage

On peu produire des textures, lisible par le pipeline graphique.

On profite des capacité du GPU (Parallélisme de donnée, tous les sommets sont calculés en même temps), mais de plus, les threads peuvent communiquer entre eux.

Les tâches/threads sont divisés en groupe qui partagent la même mémoire. 

C'est nous qui définissons le nombre de threads et de groupe par tâches.

Synchronisation des threads pour des résultats intermédiaires par la fonction barrier() et groupeMemoryBarrier().
On peut partager des variables grâce à l'attribut Shared

Opération atomique

+1 à une variable = 3 instruction read/calculate/write 
Ce genre d'opération courante sont appelées opération atomique

Certain donnée CPU et GPU ne sont pas exprimé de la même façon. 

Pour passer les valeurs entre ces unités, le compilateur convertie les données (un vec3 cpu = 3 float, vec3 gpu = 3 float aligné sur des multiples de 4)